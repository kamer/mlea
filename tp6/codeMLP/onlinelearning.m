clear;
%%============ Distance entre les deux lunes
dist = -5;
%%============ Nombre de neurones dans la couche cach?e
nbHidden = 20;
number_inputs = 2;
number_outputs = 1;
fctout = {'hyperb', 'hyperb'};
hidden = [number_inputs nbHidden number_outputs];

%%============ Pas d'apprentissage
Mepoc = 100;


nTrain = 1000;

eta = linspace(.1, 1E-5, Mepoc);
mseThreshold = 1E-3;

%%============ Generation de 2 * nTrain points
[fullData fullY] = genDeuxLunes(dist, nTrain);
%fullData=fullData+2*randn(size(fullData));

%%============ nTrain pour l'apprentissage
inputData = fullData(:, 1:nTrain);
y = fullY(1:nTrain);

%%============ nTrain pour le test
testData = fullData(:,nTrain+1:2*nTrain);
testY=fullY(nTrain+1:2*nTrain);

%%=========== Data normalization
MEAN=mean(inputData, 2);
dataCentred = inputData - repmat(MEAN, 1, nTrain);
MAX = max(abs(dataCentred), [], 2);
dataNormalized = dataCentred ./ repmat(MAX, 1, nTrain);

%%=========== Initialisation des poids du MLP
[w1 w2] = mlpinit(nbHidden);
net = mlp(hidden, fctout);

%%=========== Entrainement on line
mseTrain = Inf;
epoc = 1;
while mseTrain > mseThreshold && epoc <= Mepoc
    fprintf('  Eta : %f Epoch : %d  ', eta(epoc), epoc);
    shuffleSeq = randperm(nTrain);
    mseTrain = 0;
    %for i = 1:nTrain,
    %    %[w1 w2 e1] = mlpOnLinefit(dataNormalized(:,shuffleSeq(i)), y(shuffleSeq(i)), w1, w2, eta(epoc));
    %    [net e2] = train_mlp(net, dataNormalized(:,shuffleSeq(i)), y(shuffleSeq(i)), eta(epoc));
    %    mseTrain = mseTrain + e2^2;
    %    %disp(sprintf('%f %f', e1, e2));
    %end
    [net mseTrain] = train_mlp_batch(net, dataNormalized, y, eta(epoc));
    error(epoc) = mseTrain;
    fprintf('MSE = %f Step = %f\n', mseTrain, eta(epoc));
    epoc = epoc + 1;
end
w1 = net.w{1}';
w2 = net.w{2}';

%%================= Colormaping the figure 
figure;
hold on;
xmin = min(inputData(1,:));
xmax = max(inputData(1,:));
ymin = min(inputData(2,:));
ymax = max(inputData(2,:));

[x_b,y_b]= meshgrid(xmin:(xmax-xmin)/100:xmax,ymin:(ymax-ymin)/100:ymax);
z_b= zeros(size(x_b));
for i = 1:size(x_b,1),
    for j = 1:size(x_b, 2)
        %z_b(i,j)  = mlpval(([x_b(i,j);y_b(i,j)] - MEAN ) ./ MAX, w1, w2);
        out = mlpfwd(net, ([x_b(i,j);y_b(i,j)] - MEAN ) ./ MAX);
        out = out{net.nb_layers};
        z_b(i, j) = out;
    end
end
sp = pcolor(x_b,y_b,z_b);
load red_black_colmap;
colormap(red_black);
shading flat;
set(gca,'XLim',[xmin xmax],'YLim',[ymin ymax]);

%%============== Plot Test Data 
o=zeros(nTrain, 1);
for i = 1:nTrain,
    %o(i)=mlpval((testData(:,i) - MEAN) ./ MAX, w1, w2);
    out =mlpfwd(net, (testData(:,i) - MEAN) ./ MAX);
    out = out{net.nb_layers};
    o(i)= out;
end
plot(testData(1, o >= 0),testData(2, o >= 0),'b+','MarkerSize',8);
plot(testData(1, o < 0),testData(2, o < 0),'gx','MarkerSize',8); 
    

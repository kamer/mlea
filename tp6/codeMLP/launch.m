number_inputs = 3;
number_outputs = 1;
hidden = [number_inputs 4 3 number_outputs];

fctout = {'logistic', 'logistic', 'logistic'};

net = mlp(hidden, fctout);

x = rand(number_inputs, 1);
y = ones(number_outputs, 1);
net = train_mlp(net, x, y, 0.1);

function [X,Y]=genDeuxLunes(d, N)
% G�n�ration al�atoire de deux classes de points 2D
% sous forme de lune espac�es de d 

% INPUTS:
% d: distance entre les deux lunes


% OUTPUTS:
% X: matrice (2 x 2N) : Les coordonn�es des points 2D  
% Y: vecteur (2N X 1) : +1 pour la lune du haut et -1 la lune du bas

% Rayon et largeur
r = 10;
w = 6;

%%=================== Premi�re lune =======================================

% G�naration al�atoire des rayons des points entre r-w/2 et r + w/2
R=(r-w/2)*ones(1,N) + rand(1,N)*w;

% G�n�ration al�atoire des angles entre 0 et pi
theta=rand(1,N)*pi;

% Calcul des coordonn�es des points
X=[R.*cos(theta);R.*sin(theta)];
Y=ones(1,N);

%%================== Deuxi�me lune ========================================

% G�naration al�atoire des rayons des points entre r-w/2 et r + w/2
R=(r-w/2)*ones(1,N) + rand(1,N)*w;

% G�n�ration al�atoire des angles entre 0 et -pi
theta=-rand(1,N)*pi;

% D�placer les points de dx et dy
dx=r;
dy=-d;
x=[R.*cos(theta)+dx; R.*sin(theta)+dy];
y=-ones(1,N);

%%================ les deux lunes en un seul vecteur
X=[X x];
Y=[Y y];

%%=============== Permutation al�atoire de points
seq=randperm(2*N);
X=X(:,seq);
Y=Y(seq);

%%=============== Plot des donn�es
figure;
plot(X(1,Y==1),X(2, Y==1),'r+','MarkerSize',8); hold on;
plot(X(1,Y==-1),X(2, Y==-1),'bx','MarkerSize',8); 
axis tight;
axis equal;
title(['2 moons dataset : r=' num2str(r) ...
       ' w=' num2str(w) ' d=' num2str(d) ' N=' num2str(N)]);

function [net e] = train_mlp(net, x, y, eta)
[out act] = mlpfwd(net, x);
e = out{net.nb_layers} - y;
delta = mlpbkw(net, e, act);
o = x;
for i = 1: net.nb_layers-1
    % Ignore bias since it does not contribute to previous neurons
    d = delta{i}(1:end-1);
    net.w{i} = net.w{i} - eta .* ([o; 1] * d');
    o = out{i};
end
i = net.nb_layers;
d = delta{i};
net.w{i} = net.w{i} - eta .* ([o; 1] * d');
%e = norm(e);
end

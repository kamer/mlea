function PerceptronOnLine()
clear;

eta = 1;
nTrain = 10;

%%============ Generation de points
[Data Y] = genData(nTrain);

figure;
plotData(Data, Y);

%%=========== Data normalization
MEAN = mean(Data, 2);
DataCentred = Data - repmat(MEAN, 1, nTrain);
MAX = max(abs(DataCentred), [], 2);
DataNormalized = DataCentred ./ repmat(MAX, 1, nTrain);

%%=========== Initialisation des poids du Perceptron
W = rand(3, 1)

xmin = min(Data(1,:));
xmax = max(Data(1,:));
ymin = min(Data(2,:));
ymax = max(Data(2,:));

[xp, yp] = meshgrid(xmin:(xmax-xmin)/100:xmax,ymin:(ymax-ymin)/100:ymax);
Mesh = [xp(:)'; yp(:)'];
plotline(Mesh, MEAN, MAX, W);
pause;

%%=========== Entrainement on line
nbErr = nTrain;
epoc = 1;
while nbErr > 0 
    shuffleSeq = randperm(nTrain);
    nbErr = 0;
    for i = 1:nTrain,
        out = PercEval(DataNormalized(:,shuffleSeq(i)), W);
        if (Y(shuffleSeq(i)) ~= out)
            plot(Data(1,shuffleSeq(i)),Data(2,shuffleSeq(i)), '*g');
            W = W + eta * (Y(shuffleSeq(i)) - out) * [1; DataNormalized(:,shuffleSeq(i))];
            pause;
            hold off;
            plotData(Data, Y);
            plotline(Mesh, MEAN, MAX, W);
            nbErr = nbErr + 1;
        end
    end
    fprintf('Epoque : %d NbErr = %f\n', epoc, nbErr);
    epoc = epoc + 1;
end

fprintf('L''algorithme Converge : \n');
W
end

function [d, y] = genData(nTrain)
    d = rand(2, nTrain);
    W = [-1; 1; 1];
    y = PercEval(d, W);
end

function y = PercEval(X, W)
    s = size(X, 2); 
    y = sign(W' * [ones(1, s); X]);
end

function plotData(data, y)
    ind = find(y > 0);
    plot(data(1,ind), data(2, ind), '+r');
    hold on;
    ind = find(y <= 0);
    plot(data(1,ind), data(2, ind), 'ob');
end

function plotline(Mesh, MEAN, MAX, W)
    d = (Mesh - repmat(MEAN, 1, size(Mesh, 2))) ./ repmat(MAX, 1, size(Mesh, 2));
    Zp = reshape(PercEval(d, W), 101, 101);
    Xp = reshape(Mesh(1,:),101, 101);
    Yp = reshape(Mesh(2, :), 101, 101);
    contour(Xp(1, :), Yp(:,1), Zp, [0, 0]);
end
    

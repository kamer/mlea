function y = hyperb(x)
y = (exp(2*x)-1)./(exp(2*x)+1);
end
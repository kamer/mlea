function res = dlogistic(x)
res = logistic(x) .* (1 - logistic(x));
end

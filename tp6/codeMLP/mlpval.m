function [y h] = mlpval(x, w1, w2)
a1 = w1 * [x; 1];
h = hyperb(a1);
a2 = w2 * [h; 1];
y = hyperb(a2);
end
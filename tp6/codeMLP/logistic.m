function res = logistic(x)
res = 1 ./ (1 + exp(-x));
end

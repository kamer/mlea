function res = dmytan(v)
res = 1 - tanh(v) .^ 2;
end

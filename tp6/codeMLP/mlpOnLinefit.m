function [w1 w2 e] = mlpOnLinefit(data, y, w1, w2, step)

% new_in x 1
o1 = w1 * [data; 1];
a1 = tanh(o1);
% 1 x 1
o2 = w2 * [a1; 1];
a2 = tanh(o2);
delta2 = a2 - y;
w2_tmp = w2 - step .* (delta2 * [a1; 1]');

delta1 = w2' * delta2 .* d_hyperb([a1; 1]);
% delta1 has error of bias node which is useless
w1 = w1 - step .* (delta1(1:end-1) * [data; 1]');
w2 = w2_tmp;

e = norm(delta2);

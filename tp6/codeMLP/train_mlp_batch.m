function [net e_tot] = train_mlp_batch(net, data, label, eta)
do_batch = true;

% Cantains the big delta over whole train_set
batch = cell(1, net.nb_layers);
first = true;
e_tot = 0;
for j=1:length(data)
    x = data(:, j);
    y = label(j);
    [out act] = mlpfwd(net, x);
    e = out{net.nb_layers} - y;
    e_tot = e_tot + e ^ 2;
    delta = mlpbkw(net, e, act);
    o = x;
    for i = 1: net.nb_layers-1
        % Ignore bias since it does not contribute to previous neurons
        d = delta{i}(1:end-1);
        if first || not(do_batch)
            batch{i} = ([o; 1] * d');
        else
            batch{i} = batch{i} + ([o; 1] * d');
        end
    o = out{i};
    end
i = net.nb_layers;
d = delta{i};
if first || not(do_batch)
    batch{i} = ([o; 1] * d');
else
    batch{i} = batch{i} + ([o; 1] * d');
end
first = false;
if not(do_batch)
    for i = 1: net.nb_layers
        net.w{i} = net.w{i} - eta .* batch{i};
    end
end
end

if do_batch
    for i = 1: net.nb_layers
        net.w{i} = net.w{i} - eta .* batch{i};
    end
end
end

% Add all 3 premiere ordre step  ideally also 2 order
% Then do batch and iterative
function [out, act] = mlpfwd(net, x)
% act = activation
out = cell(1, net.nb_layers);
act = cell(1, net.nb_layers);
for i=1:net.nb_layers
    act{i} = net.w{i}' * [x; 1];
    out{i} = feval(net.fctout{i}, act{i});
    x = out{i};
end
end

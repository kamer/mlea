function res = d_hyperb(v)

res = 1 - tanh(v) .^ 2;

end

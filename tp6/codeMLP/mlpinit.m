function [w1 w2] = mlpinit(m)

w1 = randn(m, 3);
w2 = randn(1, m+1);

end
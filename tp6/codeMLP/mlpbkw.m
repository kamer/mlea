%dfct = derivee fun d'activation

% mlp backward
% activation = V
% phi(v) = final output
function delta = mlpbkw(net, e, act)
delta = cell(1, net.nb_layers);
i = net.nb_layers;
delta{net.nb_layers} = e .* feval(['d', net.fctout{i}], act{i});
d = delta{net.nb_layers};
for i = net.nb_layers - 1:-1:1
    delta{i} = (net.w{i+1} * d) .* feval(['d', net.fctout{i}], [act{i}; 1]);
    d = delta{i}(1:end-1);
end
end

% Label and Train from Tp1
clear
close all
load label.mat;
load train.mat;
train = double(train);
disp('Doing a PCA');
coeff = pca(train');
disp('Using projected PCA data');
projected = train' * coeff;

clear train;

% Data is now colon based i-e each colum has one example
data = projected(:, 1:250)';
whos data
for i=0:9
    label_nn(i+1, find(label == i)) = 1;
end

nn

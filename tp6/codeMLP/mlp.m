function net = mlp(hidden, fctout)
% hidden = [nin, 4, 3 2];
% fctout is { 'linear' &touch 'logistic', softmax');
% a cell used to describe what type of layer is used like
% { 'atanh', 'atanh', 'softmax' }
% 4 mid 3 mid 2 output
% nin number of inputs

net.nb_layers = length(hidden) - 1;
net.hidden = hidden;

w = cell(1, net.nb_layers);
for i=1:net.nb_layers
    % Number of inputs + bias
    nb_in = hidden(i) + 1;
    % Number of outputs
    nb_out = hidden(i+1);
    w{i} = randn(nb_in, nb_out);
end
net.fctout = fctout;
net.w = w;

end

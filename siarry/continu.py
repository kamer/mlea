#! /usr/bin/python3

import math;
import sys;
import networkx as nx;
import copy;
from random import randint;
from random import random;
from random import uniform;
from random import gauss;
import matplotlib.pyplot as plt
import time

class Optimizer:
    def __init__(self, pbl):
        self.pbl = pbl;
    def run(self):
        return;
    def brute(self):
        res = [];
        sol = self.pbl.gen_rand_sol();
        score = self.pbl.cost(sol);
        better = True;
        while better:
            better = False;
            for i in range(1000 * self.pbl.dim):
                cpy = list(sol);
                self.pbl.permute_uniform(sol);
                #self.pbl.permute_gauss(sol);
                newScore = self.pbl.cost(sol);
                res.append(newScore);
                if newScore >= score:
                    sol = cpy;
                else:
                    better = True;
                    score = newScore;
        print(score);
        print(sol);
        return res;

    def averageShift(self, nbTest):
        sol = self.pbl.gen_rand_sol();
        score = self.pbl.cost(sol);
        delta = 0;
        for i in range(nbTest):
            self.pbl.permute_uniform(sol);
            #self.pbl.permute_gauss(sol);
            newScore = self.pbl.cost(sol);
            delta += abs(score - newScore);
            score = newScore;
        return delta / nbTest;

    def recuit(self, tau):
        res = [];
        de = self.averageShift(100);
        fixed = 3;
        t0 = -de / math.log(tau);
        sol = self.pbl.gen_rand_sol();
        while fixed:# and math.exp(-de/t0) > 0.001:
            change = self.update(t0, sol, res);
            if not change:
                fixed -= 1;
            else:
                fixed = 3;
            t0 = 0.9 * t0;
            res.append(self.pbl.cost(sol));
            print(self.pbl.cost(sol), sol);
        print(self.pbl.cost(sol));
        return res;

    def update(self, t, sol, res):
        size = (self.pbl.max - self.pbl.min) * self.pbl.dim;
        succ = int(12 * size * size);
        fail = int(100 * size * size);
        score = self.pbl.cost(sol);
        tmp = score;
        change = False;
        while succ and fail:
            cpy = list(sol);
            self.pbl.permute_uniform(sol);
            #self.pbl.permute_gauss(sol);
            newScore = self.pbl.cost(sol);
            #res.append(newScore);
            if newScore >= score:
                fail -= 1;
                diff = newScore - score;
                if (math.exp(-diff/t) < random()):
                    sol[:] = cpy;
                else:
                    score = newScore;
                    change = True;
            else:
                succ -= 1;
                score = newScore;
                change = True;
        return change;

class problem:
    def __init__(self, dim, min, max, f):
        self.dim = dim;
        self.min = min;
        self.max = max;
        self.f = f;
        self.eps = (max - min) / 1000;
    def cost(self, sol):
        return self.f(sol);
    def permute_gauss(self, sol):
        idx = randint(0, len(sol) - 1);
        sol[idx] = gauss(sol[idx], self.eps);
        sol[idx] = max(sol[idx], self.min);
        sol[idx] = min(sol[idx], self.max);
    def permute_uniform(self, sol):
        idx = randint(0, len(sol) - 1);
        sol[idx] = uniform(sol[idx] - self.eps, sol[idx] + self.eps);
        sol[idx] = max(sol[idx], self.min);
        sol[idx] = min(sol[idx], self.max);
    def gen_rand_sol(self):
        return [uniform(self.min, self.max) for a in range(self.dim)];

def djf1(sol):
    res = 0;
    for xi in sol:
        res += xi * xi;
    return res;

def sh(sol):
    res = 0;
    for xi in sol:
        res -= xi * math.sin(math.sqrt(abs(xi)));
    return res;

def mz(sol):
    res = 0;
    for i in range(len(sol)):
        xi = sol[i];
        res += -math.sin(xi) * \
                math.pow(math.sin((i+1) * xi * xi / math.pi), 20);
    return res;


def main():
    f = problem(2, -5.12, 5.12, djf1);
    #f = problem(2, 0, math.pi, mz);
    #f = problem(2, -500, 500, sh);
    opt = Optimizer(f);
    res = opt.brute();
    #res = opt.recuit(0.8);
    print(max(res));
    plt.plot(res);
    plt.xlabel('Nombre d\'itération');
    plt.ylabel('Coût de la fonction');
    plt.title('Evolution de la descente itérative pour Michalewicz en 2D');
    plt.show();

if __name__ == "__main__":
    main();

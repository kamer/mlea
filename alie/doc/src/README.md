OpenGL
======

# Usage
```
./main [-f] [-x widthxheight]
```

Options:
*    -f : make full screen
*    -x : change screen resolution

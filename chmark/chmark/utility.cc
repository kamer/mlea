#include <math.h>
#include <iostream>

#include "utility.hh"

void chmark::print_vec3f(FIRGBF* vec)
{
  std::cout << vec->red << ", " << vec->green << ", " << vec->blue
            << std::endl;
}

void chmark::print_vec3b(RGBQUAD* vec)
{
  std::cout << (int) vec->rgbRed << ", " << (int) vec->rgbGreen
            << ", " << (int) vec->rgbBlue << std::endl;
}

float chmark::normal(const FIRGBF*x, const FIRGBF* mu, const FIRGBF* var)
{
  float red = 1.f / sqrtf(var->red * 2 * M_PI) *
               expf(-powf(x->red - mu->red, 2) / (2.f * var->red));
  float green =  1.f / sqrtf(var->green * 2 * M_PI) *
               expf(-powf(x->green - mu->green, 2) / (2.f * var->green));
  float blue =  1.f / sqrtf(var->blue * 2 * M_PI) *
               expf(-powf(x->blue - mu->blue, 2) / (2.f * var->blue));
  return red * green * blue;
  //  return -(logf(red) + logf(green) + logf(blue));
}

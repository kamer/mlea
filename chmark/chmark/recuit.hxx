#include <cassert>
#include <iostream>
#include <sstream>
#include <cmath>
#include <FreeImage.h>

#include "recuit.hh"

template <typename System>
chmark::recuit<System>::recuit(System system)
  : system_(system)
{
  std::cerr << "Learning Prior" << std::endl;
  system_.learn_prior(PROJECT_SOURCE_DIR "/data/input.jpg",
                      PROJECT_SOURCE_DIR "/data/input_ref.png");

}

template <typename System>
bool chmark::recuit<System>::update(float temp)
{
  bool has_changed = false;
  unsigned int N = 25;
  unsigned int succ = N * 12;
  unsigned int fail = N * 100;
  float energy = system_.compute_energy();
  while (succ && fail)
    {
      system_.random_swap();
      float new_energy = system_.compute_energy();
      if (new_energy < energy)
        {
          has_changed = true;
          energy = new_energy;
          --succ;
        }
      else
        {
          --fail;
          float diff = new_energy - energy;
          float rand = erand48(x_);
          if (expf(-diff/temp) < rand)
            {
              // has_changed = true;
              system_.cancel_swap();
            }
          else
            {
              energy = new_energy;
            }
        }
    }
  std::cerr << "Energy: " << energy << "\tTemp: " << temp << std::endl;
  return has_changed;
}

template <typename System>
const System& chmark::recuit<System>::get_system()
{
  return system_;
}

template <typename System>
void chmark::recuit<System>::run()
{
  std::cerr << "Computing average shift" << std::endl;
  float shift = average_shift();
  float tau = 0.8;
  float temp = -shift / logf(tau);
  unsigned int fixed = 3;
  while (fixed)
    {
      static int id = 0;
      std::cerr << "Iteration: " << id++ << "\t";
      bool has_changed = update(temp);
      if (has_changed)
        fixed = 3;
      else
        --fixed;
      temp *= 0.9;
    }
}

template <typename System>
float chmark::recuit<System>::average_shift(unsigned int nb_iter)
{
  float energy = system_.compute_energy();
  float delta = 0.f;
  for (int i = 0; i < nb_iter; ++i)
    {
      system_.random_swap();
      float new_energy = system_.compute_energy();
      delta += energy - new_energy;
      energy = new_energy;
    }
  return delta / nb_iter;
}

template <typename System>
void chmark::recuit<System>::say_hello()
{
  std::cerr << "Hello World!" << std::endl;
}

template <typename System>
std::string chmark::recuit<System>::get_info()
{
  std::ostringstream convert;
  convert << "Dimension " << system_.get_dimension();
  return convert.str();
}

namespace chmark
{
    template <typename System>
    auto make_recuit(System syst) -> recuit<System>
    {
      return recuit<System>(syst);
    }
}

#pragma once

#include <FreeImage.h>

namespace chmark
{
  class binarizer
  {
  public:
    binarizer(const char* file);
    ~binarizer();
    void random_swap();
    float compute_energy();
    unsigned get_dimension();
    void learn_prior(const char* in, const char* ref);
    void cancel_swap();
    float prior_proba(FIRGBF* x) const;
    float prior_proba_no(FIRGBF* x) const;
    const FIRGBF& get_mean() const;
    float basic_optimzer();
  private:
    // conv 2*size+1x2*size+1x
    float pixels_energy(unsigned int x, unsigned int y, unsigned int size);
    float pixel_energy(unsigned int x, unsigned int y);
    float clique_energy(unsigned int x, unsigned int y);
    void swap_bit(bool update_energy = true);
    void update_mean(FIRGBF* mean, float* count, RGBQUAD* in_val);
    void update_var(FIRGBF* var, float* count, RGBQUAD* in_val);

    FIBITMAP* image_;
    FIBITMAP* res_;
    FIRGBF mean_;
    FIRGBF var_;
    FIRGBF mean_no_;
    FIRGBF var_no_;
    float energy_;
    float old_energy_;
    int x_;
    int y_;
  };
}

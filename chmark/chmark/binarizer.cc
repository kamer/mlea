#include <iostream>
#include <math.h>

#include "binarizer.hh"
#include "chmark/utility.hh"

chmark::binarizer::binarizer(const char* file)
{
  mean_ = { 0, 0, 0 };
  var_ = { 0, 0, 0 };
  mean_no_ = { 0, 0, 0 };
  var_no_ = { 0, 0, 0 };
  energy_ = 0;

  auto type = FreeImage_GetFileType(file, 0);
  image_ = FreeImage_Load(FreeImage_GetFileType(file, 0), file);
  res_ = FreeImage_Allocate(FreeImage_GetWidth(image_),
                            FreeImage_GetHeight(image_),  8);
}

float chmark::binarizer::pixels_energy(unsigned int x_pos, unsigned int y_pos,
                                       unsigned int size)
{
  if (x_pos < 2 || y_pos < 2 || x_pos + 2 >= FreeImage_GetWidth(image_) ||
      y_pos + 2 >= FreeImage_GetHeight(image_))
    return pixel_energy(x_pos, y_pos);
  float res = 0.f;
  for (unsigned int y = y_pos - size; y <= y_pos + size; ++y)
    for (unsigned int x = x_pos - size; x <= x_pos + size; ++x)
      {
        res += pixel_energy(x, y);
      }
  return res;
}

float chmark::binarizer::clique_energy(unsigned int x_pos, unsigned int y_pos)
{
  float lambda = 0.1f;
  float res = 0.f;
  if (x_pos < 2 || y_pos < 2 || x_pos + 2 >= FreeImage_GetWidth(image_) ||
      y_pos + 2 >= FreeImage_GetHeight(image_))
    return 0.f;
  BYTE white_conv[] = {
    255, 255, 255, 255, 255,
    255, 255, 255, 255, 255,
    255, 255, 255, 255, 255,
    255, 255, 255, 255, 255,
    255, 255, 255, 255, 255
  };
  float weight[] = {
    0, 0, 0, 0, 0,
    0, 0.0f, 0.5f, 0.0f, 0,
    0, 0.5f, 0.f, 0.5f, 0,
    0, 0.0f, 0.5f, 0.0f, 0,
    0, 0, 0, 0, 0
  };
  BYTE text_conv[] = {
    255, 255, 255, 255, 255,
    255,    0,  0,   0, 255,
    255,    0,  0,   0, 255,
    255,    0,  0,   0, 255,
    255, 255, 255, 255, 255
  };
  BYTE val;
  FreeImage_GetPixelIndex(res_, x_pos, y_pos, &val);
  BYTE* conv = val == 0 ? text_conv : white_conv;
  for (unsigned int y = y_pos - 2; y <= y_pos + 2; ++y)
    for (unsigned int x = x_pos - 2; x <= x_pos + 2; ++x)
      {
        BYTE xy;
        FreeImage_GetPixelIndex(res_, x, y, &xy);
        BYTE xy_conv = conv[x - (x_pos - 2) + (y - (y_pos - 2)) * 5];
        lambda = weight[x - (x_pos - 2) + (y - (y_pos - 2)) * 5];
        lambda = 0.1;
        res += lambda * (xy != xy_conv);
      }
  return res;
}

float chmark::binarizer::pixel_energy(unsigned int x, unsigned int y)
{
  BYTE b = 0;
  float res = 0.f;
  RGBQUAD value;
  FreeImage_GetPixelColor(image_, x, y, &value);
  FIRGBF v = { (float) value.rgbRed, (float) value.rgbGreen,
               (float) value.rgbBlue };
  FreeImage_GetPixelIndex(res_, x, y, &b);
  float c = clique_energy(x, y);
  if (!b) // If black pixel then its text
    res = -logf(prior_proba(&v)) + c;
  else
    res = -logf(prior_proba_no(&v)) + c;
  return res;
}

void chmark::binarizer::random_swap()
{
  int width = FreeImage_GetWidth(image_);
  int height = FreeImage_GetHeight(image_);
  x_ = rand() % width;
  y_ = rand() % height;
  swap_bit();
}

void chmark::binarizer::swap_bit(bool update_energy)
{
  // Compute new energy every time the system changes
  // This allows constant time in update energy
  float pe;
  if (update_energy)
    pe = pixels_energy(x_, y_, 2);
  BYTE color;
  FreeImage_GetPixelIndex(res_, x_, y_, &color);
  color = !color * 255;
  FreeImage_SetPixelIndex(res_, x_, y_, &color);
  if (update_energy)
    {
      float new_pe = pixels_energy(x_, y_, 2);
      old_energy_ = energy_;
      energy_ = energy_ - pe + new_pe;
    }
}

const FIRGBF& chmark::binarizer::get_mean() const
{
  return mean_;
}

void chmark::binarizer::cancel_swap()
{
  swap_bit(false);
  energy_ = old_energy_;
}

void chmark::binarizer::update_var(FIRGBF* var, float* count,
                                   RGBQUAD* in_val)
{
  *count = *count + 1;
  var->red = var->red * (*count - 1.f) / *count +
               powf(in_val->rgbRed - mean_.red, 2) / *count;
  var->green = var->green * (*count - 1.f) / *count +
               powf(in_val->rgbGreen - mean_.green, 2) / *count;
  var->blue = var->blue * (*count - 1.f) / *count +
                powf(in_val->rgbBlue - mean_.blue, 2) / *count;
}

void chmark::binarizer::update_mean(FIRGBF* mean, float* count,
                                    RGBQUAD* in_val)
{
  *count = *count + 1;
  mean->red = mean->red * (*count - 1.f) / *count + in_val->rgbRed / *count;
  mean->green = mean->green * (*count - 1.f) / *count +
                in_val->rgbGreen / *count;
  mean->blue = mean->blue * (*count - 1.f) / *count + in_val->rgbBlue / *count;
}

void chmark::binarizer::learn_prior(const char* in, const char* ref)
{
  FIBITMAP* ref_img = FreeImage_Load(FreeImage_GetFileType(ref, 0), ref);
  FIBITMAP* in_img = FreeImage_Load(FreeImage_GetFileType(in, 0), in);
  unsigned int width = FreeImage_GetWidth(ref_img);
  unsigned int height = FreeImage_GetHeight(ref_img);
  float count = 0.f;
  float count_no = 0.f;
  for (unsigned int y = 0; y < height; ++y)
    for (unsigned int x = 0; x < width; ++x)
      {
        RGBQUAD val;
        FreeImage_GetPixelColor(ref_img, x, y, &val);
        RGBQUAD in_val;
        FreeImage_GetPixelColor(in_img, x, y, &in_val);
        if (val.rgbGreen == 0 && val.rgbRed == 0 && val.rgbBlue == 0)
          update_mean(&mean_, &count, &in_val);
        else
          update_mean(&mean_no_, &count_no, &in_val);
      }

  count = 0.f;
  count_no = 0.f;
  for (unsigned int y = 0; y < height; ++y)
    for (unsigned int x = 0; x < width; ++x)
      {
        RGBQUAD val;
        FreeImage_GetPixelColor(ref_img, x, y, &val);
        RGBQUAD in_val;
        FreeImage_GetPixelColor(in_img, x, y, &in_val);
        if (val.rgbGreen == 0 && val.rgbRed == 0 && val.rgbBlue == 0)
           update_var(&var_, &count, &in_val);
        else
          update_var(&var_no_, &count_no, &in_val);
      }
  std::cerr << "Mean of text: "; chmark::print_vec3f(&mean_);
  std::cerr << "Variance of text: "; chmark::print_vec3f(&var_);

  std::cerr << "Mean of white: "; chmark::print_vec3f(&mean_no_);
  std::cerr << "Variance of white: "; chmark::print_vec3f(&var_no_);
}

unsigned chmark::binarizer::get_dimension()
{
  RGBQUAD value;
  FreeImage_GetPixelColor(image_, 0, 0, &value);
  BYTE b = value.rgbBlue;
  return FreeImage_GetWidth(image_) * FreeImage_GetHeight(image_);
}

float chmark::binarizer::basic_optimzer()
{
  float res = 0.f;
  unsigned int width = FreeImage_GetWidth(image_);
  unsigned int height = FreeImage_GetHeight(image_);
    for (unsigned int y = 0; y < height; ++y)
    {
      for (unsigned int x = 0; x < width; ++x)
        {
          RGBQUAD value;
          FreeImage_GetPixelColor(image_, x, y, &value);
          FIRGBF v = { (float) value.rgbRed, (float) value.rgbGreen,
                       (float) value.rgbBlue };
          BYTE b = 0;
          //chmark::print_vec3f(&v);
          //chmark::print_vec3b(&value);
          if (prior_proba(&v) > prior_proba_no(&v))
            b = 0;
          else
            b = 255;
          FreeImage_SetPixelIndex(res_, x, y, &b);
        }
    }
  return res;
}

float chmark::binarizer::compute_energy()
{
  static bool first_call = true;
  if (first_call)
    {
      first_call = false;
      float res = 0.f;
      unsigned int width = FreeImage_GetWidth(image_);
      unsigned int height = FreeImage_GetHeight(image_);
      for (unsigned int y = 0; y < height; ++y)
        {
          for (unsigned int x = 0; x < width; ++x)
            {
              res += pixel_energy(x, y);
            }
        }
      energy_ = res;
      return res;
    }
  else
    return energy_;
}

float chmark::binarizer::prior_proba(FIRGBF* x) const
{
  return chmark::normal(x, &mean_, &var_);
}

float chmark::binarizer::prior_proba_no(FIRGBF* x) const
{
  return chmark::normal(x, &mean_no_, &var_no_);
}

chmark::binarizer::~binarizer()
{
  FreeImage_Save(FIF_PNG, res_ , "filename.png" , 0);
}

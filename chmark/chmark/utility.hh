#pragma once

#include <FreeImage.h>

namespace chmark
{
  void print_vec3f(FIRGBF* vec);
  void print_vec3b(RGBQUAD* vec);
  float normal(const FIRGBF*x, const FIRGBF* mu, const FIRGBF* var);
}

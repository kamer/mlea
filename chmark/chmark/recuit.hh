#pragma once

namespace chmark
{
  /// System defines the system to be optimised it must have the following
  /// functions available:
  /// float compute_energy(); void random_swap();
  template <typename System>
  class recuit
  {
  public:
    recuit(System system);
    void say_hello();
    void run();
    std::string get_info();
    const System& get_system();
  private:
    float average_shift(unsigned int nb_iter = 1000);
    bool update(float temp);
    System system_;
    unsigned short x_[3] = {0, 0, 0};
  };

  template <typename System>
  auto make_recuit(System syst) -> recuit<System>;
}

#include "recuit.hxx"

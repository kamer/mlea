#include <iostream>

#include <Config.h>
#include <chmark/recuit.hh>
#include <chmark/binarizer.hh>

template <typename System>
void print_debug_info(System syst)
{
  std::cout << "Current version is: "
            << CHMARK_VERSION_MAJOR
            << '.' << CHMARK_VERSION_MINOR << std::endl;
  std::cout << "info:" << syst.get_info() << std::endl;
}

int main(int argc, const char* argv[])
{
  using namespace chmark;
  const char* path = PROJECT_SOURCE_DIR "/data/input.jpg";
  if (argc >= 2)
    path = argv[1];
  auto test = make_recuit(binarizer(path));
  print_debug_info(test);
  test.run();
  return 0;
}

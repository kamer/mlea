#include <iostream>

#include <Config.h>
#include <chmark/recuit.hh>
#include <chmark/binarizer.hh>

template <typename System>
void print_debug_info(System syst)
{
  std::cout << "Current version is: "
            << CHMARK_VERSION_MAJOR
            << '.' << CHMARK_VERSION_MINOR << std::endl;
  std::cout << "info:" << syst.get_info() << std::endl;
}

template <typename System>
void print_proba(const System& system, FIRGBF& tmp)
{
  std::cout << "Proba: " << system.prior_proba(&tmp)
            << " vs NoProba: " << system.prior_proba_no(&tmp) << std::endl;
}

template <typename System>
void print_probas(const System& system)
{
  FIRGBF tmp = { 0, 0, 0 };
  print_proba(system, tmp);

  tmp = { 80, 80, 80 };
  print_proba(system, tmp);

  tmp = system.get_mean();;
  print_proba(system, tmp);

  tmp = { 255, 255, 255 };
  print_proba(system, tmp);
}

int main()
{
  using namespace chmark;
  auto b = binarizer(PROJECT_SOURCE_DIR "/data/input.jpg");
  auto test = make_recuit(b);
  print_debug_info(test);
  print_probas(test.get_system());
  b.learn_prior(PROJECT_SOURCE_DIR "/data/input.jpg",
                PROJECT_SOURCE_DIR "/data/input_ref.png");
  //b.basic_optimzer();
  for (int i = 0; i < 1000; ++i)
    {
      float before = b.compute_energy();
      b.random_swap();
      b.cancel_swap();
      float after = b.compute_energy();
      assert(fabs(before - after) < 0.001);
    }
  return 0;
}

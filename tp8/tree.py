#! /usr/bin/python3

from sklearn.decomposition import PCA
from sklearn.externals.six import StringIO
import numpy as np
import pylab;
from PIL import Image;
import math;
import csv;
from load import load_data;
from sklearn import tree;

def get_pca(data):
    print("Doing PCA:");
    train_set_x, train_set_y = data[0][0], data[0][1];
    test_set_x, test_set_y =   data[1][0], data[1][1];
    valid_set_x, valid_set_y =   data[2][0], data[2][1];

    pca = PCA(n_components = 50);

    print("Fitting PCA + transform train");
    train_set_x = pca.fit_transform(train_set_x);
    print("Transform test");
    test_set_x = pca.transform(test_set_x);
    print("Transform valid");
    valid_set_x = pca.transform(valid_set_x);

    print(train_set_x.shape);
    print(test_set_x.shape);

    return [(train_set_x, train_set_y), (test_set_x, test_set_y),
            (valid_set_x, valid_set_y)];

def learn_mnist():
    data = get_pca(load_data("mnist.pkl.gz"));
    #data = load_data("mnist.pkl.gz");
    #train_set_x, train_set_y = (data[0][0] * 256).astype(int), data[0][1];
    #test_set_x, test_set_y = (data[1][0] * 256).astype(int), data[1][1];
    train_set_x, train_set_y = data[0][0], data[0][1];
    test_set_x, test_set_y =   data[1][0], data[1][1];
    Y = train_set_y;
    X = train_set_x;

    clf = tree.DecisionTreeClassifier(max_depth=10);
    print("Start learning");
    clf = clf.fit(X, Y);
    print("end learning");

    Y = test_set_y;
    X = test_set_x;
    predict = clf.predict(test_set_x);
    nb_error = (predict != test_set_y).sum();
    print("Test:", nb_error / test_set_y.shape[0] * 100);
    print("Validation:",
            (clf.predict(data[2][0]) != data[2][1]).sum() /
            data[2][1].shape[0] * 100)
    return clf;

def dot(clf):
    with open("iris.dot", 'w') as f:
        f = tree.export_graphviz(clf, out_file=f);

def main():
    #learn_tennis();
    #learn_mushroom();
    clf = learn_mnist();
    dot(clf);

if __name__ == "__main__":
    main();

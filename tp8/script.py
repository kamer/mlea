#! /usr/bin/python3

import numpy as np
import pylab;
from PIL import Image;
import math;
import csv;
from load import load_data;

g_use_split_info = True;
g_label = [ 'Outlook', 'Temperatures', 'Humidity', 'Wind', 'PlayTennis' ];
g_info = np.array([['Sunny', 'Overcast', 'Rain'],
                  ['Mild', 'Hot', 'Cool'],
                  ['High', 'Normal'],
                  ['Weak', 'Strong'],
                  ['No', 'Yes']]);
g_mushroom = False;

class node:
    def __init__(self, attr, nval, gain):
        self.attr = attr;
        self.nval = nval;
        self.suns = [];
        self.class_id = -1;
        self.gain = gain;
    def __str__(self):
        global g_mushroom;
        if not g_mushroom:
            return str(self.class_id) + '\\n' + g_label[self.attr] + '\\n' + \
                    g_info[self.attr][self.nval] + '\\n' + self.info + \
                    '\\n' + "Gain: " + self.gain;
        else:
            if (self.attr, self.nval) == (-1, -1):
                return "Root Node\\n" + str(self.class_id) + '\\n' + self.info \
                    + '\\n' + "Gain: " + self.gain;
            return str(self.class_id) + '\\n' + "Attr: " + \
                    g_label[self.attr] + '\\n' + \
                    g_info[(self.attr, self.nval)] + '\\n' + self.info \
                    + '\\n' + "Gain: " + self.gain;

    def add_sun(self, sun):
        self.suns.append(sun);

# Returns gain and slpit info
def info_gain(attr, C):
    val = np.array(C[attr]);
    nval = val.shape[0];
    nc = val.shape[1];
    res = 0;
    nb_elt = val.sum().sum();
    split_info = 0;
    for v in range(nval):
        summed_ratio = 0;
        for i in range(nc):
            p_ac = val[v][i] / nb_elt;
            # Count number of elements with value v
            p_a = val[v].sum() / nb_elt;
            # Compute sum of rations of splitting values
            summed_ratio = p_a;
            # Count number of true or false
            p_c = np.array([tmp[i] for tmp in val]).sum() / nb_elt;
            if p_ac != 0:
                res += p_ac * math.log(p_ac / (p_a * p_c), 2);
        # compute actual spit_info
        if summed_ratio != 0:
            split_info = split_info - summed_ratio * math.log(summed_ratio, 2);
    return [res, split_info];

def split(A, C):
    global g_use_split_info;
    best = -float('inf');
    res = 0;
    for attr in A:
        attr = int(attr);
        [gain, split_info] = info_gain(attr, C);
        if g_use_split_info:
            gain = gain / split_info;
        if gain > best:
            best = gain;
            res = attr;
    return [res, best];

# Split elements depending on one attribute.
# Recompute C for each attributes even those no longer used
# Continue splitting until attributes are left
# At each split should create len(C[pivot]) sons, where each
# son is a value of the attribute. Then on final node we return
# the class that is the most present
def id3(data, Y, cpy):
    print("Iteration:" + str(len(cpy)));
    if len(cpy):
        [C, info] = computeC(data, Y);
        # Pivot is the current attribute being used
        [pivot, gain] = split(cpy, C);
        # Construct root node, second param is -1 as label should be on
        # transition and not on root state
        res = node(-1, -1, str(gain));
        res.info = info;
        class_id = max(set(Y), key=list(Y).count);
        res.class_id = class_id;
        # When all labels are the same return node as is no need to expand it
        if ((Y[0] == Y).sum() == len(Y)):
            return res;

        cpy.remove(pivot);
        # Iterating over all values attribute can take
        # Each bucket of C has n values representing the n different possible
        # discrete values
        for i in range(len(C[pivot])):
            indices = np.where(data[:, pivot] == i)
            newdata = data[indices];
            newy = Y[indices];
            if len(newy) and len(newdata):
                # TODO create the tree here.
                sun = id3(newdata, newy, list(cpy));
                if sun != None:
                    sun.nval = i;
                    sun.attr = pivot;
                    res.add_sun(sun);
        return res;
    return None;

def computeC(data, Y):
    nc = int(max(Y) + 1);
    info = dict();
    for id in range(nc):
        info[id] = (Y == id).sum();
    C = list();
    for i in range(data.shape[1]):
        print(i);
        tmp = list();
        # nval is the number of discretized values
        nval = int(max(data[:, i]));
        for val in range(nval+1):
            tup = list();
            for c in range(nc):
                tup.append(((data[:, i] == val) & (Y == c)).sum());
            tmp.append(tup);
        C.append(tmp);
    return [C, str(info)];

def dot(node):
    print('digraph G {');
    id = 0;
    map = dict();
    map[node] = id;
    id = id + 1;
    todo = [node];
    while len(todo) != 0:
        node = todo.pop();
        for s in node.suns:
            map[s] = id;
            id = id + 1;
            todo.append(s);
            print('\t', map[node], ' -> ', map[s]);
    for v in map:
        print('\t', map[v], '[label="', str(v), '"]');
    print('}');

def read_mushroom():
    #csvfile = open('data/data.txt', 'r');
    csvfile = open('data/agaricus-lepiota.data', 'r');
    text = csv.reader(csvfile);
    data = list();
    d = dict();
    m = dict();
    label = dict();
    for line in text:
        arr = [];
        idx = 0;
        for word in line:
            if not((idx, word) in d.keys()):
                if idx in m.keys():
                    m[idx] = m[idx] + 1;
                else:
                    m[idx] = 0;
                d[(idx, word)] = m[idx];
                # use idx-1 because final labels do not have first column which
                # is only used for class_id values
                label[(idx-1, m[idx])] = word;
            arr.append(d[(idx, word)]);
            idx = idx + 1;
        data.append(arr);
    return [np.array(data), label];

def learn_mushroom():
    global g_mushroom;
    global g_info;
    global g_label;
    [data, label] = read_mushroom();
    Y = data[:, 0];
    data = data[:, 1:];
    nb_attr = data.shape[1];
    cpy = list(np.linspace(0, nb_attr - 1, nb_attr));
    n = id3(data, Y, cpy);
    g_info = label;
    g_label = [str(v) for v in range(100)];
    g_mushroom = True;
    dot(n);

def learn_tennis():
    data = np.loadtxt("data.txt");
    Y = data[:, -1];
    # data is stored nxd where d is number of dimention
    # Remove 1 as last dimension is label
    data = data[:, 0:-1];
    nb_attr = data.shape[1];
    cpy = list(np.linspace(0, nb_attr - 1, nb_attr));
    # Computing tree
    n = id3(data, Y, cpy);
    dot(n);

def learn_mnist():
    data = load_data("mnist.pkl.gz");
    test_set_x, test_set_y = (data[1][0] * 256).astype(int), data[1][1];
    print(test_set_x.shape);
    Y = test_set_y;
    data = test_set_x;
    nb_attr = data.shape[1];
    cpy = list(np.linspace(0, nb_attr - 1, nb_attr));
    print("computing tree");
    n = id3(data, Y, cpy);
    dot(n);

def main():
    #learn_tennis();
    #learn_mushroom();
    learn_mnist();

if __name__ == "__main__":
    main();

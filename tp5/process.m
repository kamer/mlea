function res = process(alpha, b, labelTrain, dataTrain, data, kernel)
res = sign(decision(alpha, b, labelTrain, dataTrain, data, kernel));
end

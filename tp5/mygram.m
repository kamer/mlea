function G = mygram(X, Y)
G = (Y' * Y) .* (X' * X);
end

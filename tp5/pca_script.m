gendata;
load donuts
Y = y;
kernel = @(x, y) x' * y;
sigma = 0.01;
% RBF Function
kernel = @(x, y) exp(-norm(x - y) ^ 2 / (2 * sigma^2));

coord = kpca(X, X, kernel);play
i = 1;
tmp = coord(:, [1 2]);

clf
plot(tmp(find(Y == i), 1), tmp(find(Y == i), 2), 'xr')
hold on
i = 2;
plot(tmp(find(Y == i), 1), tmp(find(Y == i), 2), 'xb')
play
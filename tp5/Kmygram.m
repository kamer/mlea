function G = Kmygram(X, Y, kernel)
n = size(X, 2);
for i = 1:n
    for j=i:n
        G(i,j) = kernel(X(:,i), X(:,j));
        G(j,i) = G(i,j);
    end;
end;

G = G .* (Y' * Y);
end

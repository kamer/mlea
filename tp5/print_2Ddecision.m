function print_2Ddecision(alpha, b, label, dataTrain, kernel)
res = process(alpha, b, label, dataTrain, dataTrain, kernel);
w = sum(repmat(alpha' .* label, size(dataTrain, 1), 1) .* dataTrain, 2);


clf
point = -b / w;
idx = find(label == -1);
idx2 = find(label == 1);
if (size(dataTrain, 1) == 1)
    plot(sort(dataTrain(idx)), 'bo', 'MarkerSize', 12, 'MarkerFaceColor', 'b');
    hold on;
    plot(sort(dataTrain(idx2)), 'ro', 'MarkerSize', 12, 'MarkerFaceColor', 'r');

    plot(ones(size(dataTrain, 2), 1) * point);
elseif (size(dataTrain, 1) == 2)
    plot(dataTrain(1, idx), dataTrain(2, idx), 'bo', 'MarkerSize', 6, 'MarkerFaceColor', 'b');
    hold on;
    plot(dataTrain(1, idx2), dataTrain(2, idx2), 'ro', 'MarkerSize', 6, 'MarkerFaceColor', 'r');

    dir = [-w(2); w(1)];
    minx = min(dataTrain(1, :));
    maxx = max(dataTrain(1, :));
    miny = min(dataTrain(2, :));
    maxy = max(dataTrain(2, :));
    xstep = (-minx + maxx) / 100;
    ystep = (-miny + maxy) / 100;
    [x, y] = meshgrid(minx:xstep:maxx, miny:ystep:maxy);
    grid = [x(:)'; y(:)'];
    res = decision(alpha, b, label, dataTrain, grid, kernel);
    l = find(sign(res) >= 0);
    r = find(sign(res) < 0);

    left = [point' - dir];
    right = [point' + dir];

    right = dir * (-maxx + right(1)) + right;
    left = dir *  (-minx + left(1)) + left;
    point = [left, right];
    %plot(point(1, :), point(2, :), 'g', 'MarkerSize', 12, 'MarkerFaceColor', 'r');
    axis square
    hold on
    plot(grid(1, l), grid(2, l), 'rx', 'MarkerSize', 12, 'MarkerFaceColor', 'r');
    plot(grid(1, r), grid(2, r), 'bx', 'MarkerSize', 12, 'MarkerFaceColor', 'b');
end
end

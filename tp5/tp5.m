% Gram Matrix
Y = [-1; 1; 1];
X = [ 1; 2; 3];

% define yy<x,x> i-e Gram matrix
G = (Y * Y') .* (X * X');
% Define linear function on learned alpha
% Note quadprog minimizes, we want to maximize so we multiply our problem by -1
f = -ones(length(X), 1);
% Define A*X <= b
A = Y;
b = 1;
% We want Y'*alphi = 0
beq = 0;
Aeq = Y;
% We want alphi > 0
LB = zeros(length(X), 1);
UP = []; % No upper bound
alpha = quadprog(G, f, A', 0, Aeq', beq, LB, UP)

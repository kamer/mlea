function [G, li] = computeM(X, Y, label, kernel)
Xtarg = X(:, find(Y == label));
m = size(Xtarg, 2);
n = size(X, 2);
G = zeros(n, 1);
for i = 1:n
    for j=1:m
        G(i) = G(i) + kernel(X(:,i), Xtarg(:,j));
    end;
end;
G = G ./ m;
li = m;

end

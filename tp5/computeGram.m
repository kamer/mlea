function [G, m] = computeGram(X, Y, label, kernel)
Xtarg = X(:, find(Y == label));
n = size(X, 2);
m = size(Xtarg, 2);
for i = 1:n
    for j=1:m
        G(i,j) = kernel(X(:,i), Xtarg(:,j));
    end;
end;

end

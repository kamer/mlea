function FEval = decision(alpha, b, labelTrain, dataTrain, data, kernel)

%repmat((label .* alpha'), size(X, 1), 1)
%W = repmat((label .* alpha'), size(X, 1), 1) .* X;

% Dot product with every other elements
g = Kgram(dataTrain, data, kernel);
%FEval = sum(repmat((labelTrain' .* alpha), 1, size(data, 2)) .* ...
%            (dataTrain' * data), 1) + b;
FEval = sum(repmat((labelTrain' .* alpha), 1, size(data, 2)) .* ...
            g, 1) + b;
end

function G = Kgram(X, Y, kernel)
n = size(X, 2);
m = size(Y, 2);
for i = 1:n
    for j=1:m
        G(i,j) = kernel(X(:,i), Y(:,j));
    end;
end;
end

clear
nc = 1;
gendata;
load donuts.mat
%load data.mat
%LABEL_TEST = LABEL_TEST + 1;
%X = double(TEST(:, :))';
nb_elt = 800;
nb_elt = length(X);
X = X(:, 1:nb_elt);
%[mat, X] = pca(X');
%X = X';
%X = X(1:50, :);
%y = LABEL_TEST;
y = y(1:nb_elt);
Y = y;
Y(find(Y == -1)) = 2;
%gendata;

sigma = 0.1;
kernel = @(x, y) exp(-norm(x - y) ^ 2 / (2 * sigma^2));
kernel = @(x, y) (1 + x' * y) ^ 1;

numx = length(find(Y == 1));
numy = length(find(Y == 2));

% M is the projection of all data in class C on each element of data.
% Compute M1 dot product of all 1/2 against vector 1 x length(X)
Mstar = computeMstar(X, kernel);
M = 0;
for i = 1:nc
    [Mi, li] = computeM(X, Y, i, kernel);
    M = M + li * (Mi - Mstar) * (Mi - Mstar)';
end

% REDAs M
K = computeK(X, kernel);
%M = computeMi(K1, K) + computeMi(K2, K);
%

% N is sum of each class
N = 0;
for i = 1:nc
    [Ki, li] = computeGram(X, Y, i, kernel);
    N = N + Ki * (eye(li) - ones(li, li) ./ li) * Ki';
end
% end compute N
mu = 1e-3;
mu = 0;
N = N + eye(length(N)) .* mu;
%N = N ./ norm(N);
alpha = pinv(N) * M;
t = alpha;
alpha = alpha(:, 1:(nc));

% COMPUTE EIGEN VECTORS
  % [V, D] = eig(M, N);
  % d = diag(D);
  % [a, idx] = sort(d, 1, 'descend');
  % d = d(idx);
  % V = V(:, idx);
  % w2 = K * V;
  % V = V(:, 1);
  % %V = V ./ norm(V);
  % alpha = V;
% END COMPUTE EIGEN VECTORS

% This computes all eigenvalues, only first one is non zero
%d = eig(inv(N) * ((M2 - M1) * (M2 - M1)'));

% Compute mean of each class
for i = 1:nc
    meani{i} = mean(X(:, find(Y == i)), 2);
    resi{i} = ldaProject(meani{i}, X, kernel, alpha);
end

n = size(X, 2);
for i = 1:n
    res(i, :) = ldaProject(X(:, i), X, kernel, alpha);
end
proj_means = repmat(resi{1}, n, 1);
tmp = sum((res - proj_means) .^ 2, 2);
for i = 2:nc
    proj_means = repmat(resi{i}, n, 1);
    tmp = [tmp, sum((res - proj_means) .^ 2, 2)];
end
res = res';
[val, idx] = min(tmp, [], 2);

result = sum(idx(:) ~= Y(:));
disp(sprintf('Error is %f', 100 * result / n));

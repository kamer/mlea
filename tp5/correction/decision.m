function FEval = decision(kernel, alpha, dataTrain, labelTrain, b,  data)

IndSV = find(alpha > 1e-5);
% Compute f(x) = sum_i_in_sv alpha_i label_i K(dataTrain_i, x) + b
G = computeGramSV(data, dataTrain(:,IndSV), kernel);
FEval = (alpha(IndSV)' .* labelTrain(IndSV)) * G' + b;
end

function G = computeGramSV(X, SV, ker)

[dim n] = size(X);
[dim nSV] = size(SV);

G=zeros(n,nSV);
for i = 1:n
    for j=1:nSV
        G(i,j) = feval(ker,X(:,i), SV(:,j));
    end;
end;
end


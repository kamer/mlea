function print2Ddecision(kernel, alpha, dataTrain, labelTrain, b)

% Plot Data 
plotData(dataTrain, labelTrain);


% get axis
a = axis;
old_hold = ishold;
hold on;

% limits of current figure
xmin=a(1);
xmax=a(2);
ymin=a(3);
ymax=a(4);

% makes grid
[X,Y] = meshgrid(xmin:(xmax-xmin)/100:xmax, ymin:(ymax-ymin)/100:ymax);

% generate samples
testData = [reshape(X',1,prod(size(X))); reshape(Y',1,prod(size(Y)))];

% classify points
svmfun = decision(kernel, alpha, dataTrain, labelTrain, b, testData);

% compute color limits
l = (-min(svmfun) + max(svmfun))/2;

%reshape svmfun
Z = reshape(svmfun, size(X,1), size(X,2))';

% colors background
hback = pcolor(X,Y,Z);
% smooth shading
shading interp;

% plots decision boundary
[tmp, h] = contour(X,Y,Z,[0,0],'k');
set(h,'LineWidth',2);

% plots margins
[tmp, h] = contour(X,Y,Z,[1,1],'k--');
set(h,'LineWidth',2);
[tmp, h] = contour(X,Y,Z,[-1,-1],'k--');
set(h,'LineWidth',2);


% set color limits and colormap
set(hback, 'LineStyle','none' );
set(gca,'Clim',[-l l]);

% creates colormap
t=linspace(1,0.25,32)';
cmp=zeros(64,3);
cmp(1:32,1)=1;
cmp(1:32,2:3)=[flipud(t) flipud(t)];
cmp(33:end,3)=1;
cmp(33:end,1:2)=[t t];
colormap(cmp)

% Plot SV
indSV = find(alpha > 1e-5);
plotDataClass(dataTrain(:,indSV), 'ok');

if ~old_hold, hold off; end
end

function plotData(data, label)
old_hold = ishold;
hold on;
ind = find(label > 0);
plotDataClass(data(:,ind), 'bx');
ind = find(label < 0);
plotDataClass(data(:,ind), 'r+');
if ~old_hold, hold off; end
end

function h = plotDataClass(data, marker)

h = plot(data(1,:), data(2,:), marker);
set(h, 'LineWidth', 2);
set(h, 'MarkerSize', 6);

end

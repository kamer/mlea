function G = computeGram(X, ker)

[dim n] = size(X);

G=zeros(n,n);
for i = 1:n
    for j=i:n
        G(i,j) = feval(ker,X(:,i), X(:,j));
        G(j,i) = G(i,j);
    end;
end;
end


function [alpha, b, Marge, Err] = Htrain(G, label , C)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
n=size(G,1);
H = (label(:) * label(:)') .* G;
f = -ones(n,1);
Aeq = label(:)';

opt = optimoptions('quadprog', 'Algorithm', 'interior-point-convex');
[alpha fval exitflag output lambda]= quadprog(H, f, [], [], Aeq, 0, zeros(n,1), C * ones(n,1), [], opt);

if (exitflag ~= 1)
    error ('quadprog ne converge pas exitFlag : ', exitflag);
end;

Marge = 2 / sqrt(sum(sum((alpha(:) * alpha(:)') .* H)));

%%%%%%%%%%%%%%%%
% Compute b
%%%%%%%%%%%%%%%%
IndSV = find(alpha > 1e-5);
b = sum(label(IndSV) - (alpha(IndSV)' .* label(IndSV)) * G(IndSV, IndSV)) / length(IndSV);

%%%%%%%%%%%%%%%%%
% Compute Err
%%%%%%%%%%%%%%%%%
feval = sum(repmat(alpha, 1, n) .* repmat(label(:), 1, n) .* G) + b;
class = sign(feval);
Err = sum(class(:) ~= label(:));
end


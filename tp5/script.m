clear all
%gendata;
%G = mygram(X, Y);
%[alpha, b, margin, Err] = Htrain(G, Y);
% Passing labeltrain and datatrain and data to test
%FEval = decision(alpha, b, Y, X, [1, 1.5, 2; 1, 1.5, 2]);
%res = process(alpha, b, Y, X, [1, 1.5, 2; 1, 1.5, 2]);
%print_2Ddecision(alpha, b, Y, X);

gendata;
%load donuts.mat
Y = y;
Y(find(Y == 2)) = -1;
kernel = @(x, y) (1 + x' * y) ^ 2;
sigma = 0.1
% RBF Function
kernel = @(x, y) exp(-norm(x - y) ^ 2 / (2 * sigma^2));
G = Kmygram(X, Y, @(x, y) x' * y);
G = Kmygram(X, Y, kernel);
[alpha, b, margin, Err] = Htrain(G, Y);
% Passing labeltrain and datatrain and data to test
% FEval = decision(alpha, b, Y, X, [1, 1.5, 2; 1, 1.5, 2]);
% res = process(alpha, b, Y, X, [1, 1.5, 2; 1, 1.5, 2]);
print_2Ddecision(alpha, b, Y, X, kernel);

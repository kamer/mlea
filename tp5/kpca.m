function coordPCA = kpca(dataTrain, dataTest, kernel)
%G = computeGram(data, ones(length(data)), 1, kernel);
%% C chapeau
%chat = G - repmat(sum(G, 2) / N, 1, N)
%         - repmat(sum(G, 1) / N, N, 1)
%         + sum(sum(G)) / N ^ 2;
%[U, D] = eig(chat);
%norm = diag(U' * chat * U);
%U = U * diag(1 ./ sqrt(norm));
%[d, I] = sort(diag(D), 1, 'descend');
%U = U(:, I);
%coordData = chat * U;

%data Test
G = computeGram(dataTrain, ones(length(dataTrain), 1), 1, kernel);
N1 = length(dataTest);
N = length(dataTrain);
Gtest = zeros(N1, N);
for i = 1:N1
    for j = 1:N
        Gtest(i, j) = kernel(dataTest(:, i), dataTrain(:, j));
    end
end
M = Gtest - repmat(sum(Gtest, 2) / N, 1, N) ...
          - repmat(sum(G, 1) / N, N, 1) ...
          + sum(sum(G)) / N^2;
M = (M + M') / 2;
[U, D] = eig(M);
norm = diag(U' * M * U);
U = U * diag(1 ./ sqrt(norm));
[d, I] = sort(diag(D), 1, 'descend');
U = U(:, I);

coordPCA = M * U;
end

nc = 10;
gendata;
load donuts.mat
load data.mat
LABEL_TEST = LABEL_TEST + 1;
LABEL_TRAIN = LABEL_TRAIN + 1;
X = double(TEST(:, :))';
X = double(TRAIN(:, :))';
nb_elt = 800;
nb_elt = length(LABEL_TEST);
nb_elt = length(LABEL_TRAIN);
X = X(:, 1:nb_elt);
[mat, X] = pca(X');
X = X';
X = X(1:50, :);
y = LABEL_TEST;
y = LABEL_TRAIN;
y = y(1:nb_elt);
Y = y;
Y(find(Y == -1)) = 2;


m = mean(X, 2);
Sw = 0;
Sb = 0;
for i = 1:nc
    ni = length(find(Y == i));
    Xi = X(:, find(Y == i));
    mi = mean(Xi, 2);
    mm = repmat(mi, 1, size(Xi, 2));
    Sw = Sw + (Xi - mm) * (Xi - mm)';
    Sb = Sb + ni * (mi - m) * (mi - m)';
end

%numx = length(find(Y == 1));
%numy = length(find(Y == -1));
%
%X1 = X(:, find(Y == 1));
%X2 = X(:, find(Y == -1));
%m1 = repmat(mean(X1, 2), 1, size(X1, 2));
%m2 = repmat(mean(X2, 2), 1, size(X2, 2));
%Sw = (X1 - m1) * (X1 - m1)';
%Sw = Sw + (X2 - m2) * (X2 - m2)';
%Sb = (m2(:, 1) - m1(:, 1)) * (m2(:, 1) - m1(:, 1))';

[V, D] = eig(Sb, Sw);
d = diag(D);
[a, idx] = sort(d, 'descend');
d = d(idx);
V = V(:, idx);
V = V(:, 1:(nc - 1));
V = V ./ norm(V);

%w = inv(Sw) * (m2(:, 1) - m1(:, 1));
for i = 1:nc
    m = mean(X(:, find(Y == i)), 2);
    resi{i} = (V' * m)';
end

res = V' * X;
res = res';

proj_means = repmat(resi{1}, size(X, 2), 1);
tmp = sum((res - proj_means) .^ 2, 2);
for i = 2:nc
    proj_means = repmat(resi{i}, size(X, 2), 1);
    tmp = [tmp, sum((res - proj_means) .^ 2, 2)];
end
[val, idx] = min(tmp, [], 2);

result = sum(idx(:) ~= Y(:));
disp(sprintf('Error is %f', 100 * result / size(X, 2)));

% eig(Sb, Sw) = eig(inv(Sw) * Sb);
% Sb w = lambda Sw w ==> w = inv(Sw) * Sb

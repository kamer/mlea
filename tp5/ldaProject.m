function res = ldaProject(target, data, kernel, alpha)
n = size(data, 2);
res = 0;
for i = 1:n
    %kernel(target, data(:, i));
    res = res + alpha(i, :) * kernel(target, data(:, i));
end
end

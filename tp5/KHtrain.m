function [alpha, b, margin, Err] = KHtrain(G, label)
% Define linear function on learned alpha
% Note quadprog minimizes, we want to maximize so we multiply our problem by -1
f = -ones(1, length(label));
% Define A*X <= b
A = label;
b = 0;
% We want Y'*alphi = 0
beq = 0;
Aeq = label;
% We want alphi > 0
LB = zeros(1, length(label));
UP = ones(1, length(label)) * 40; % No upper bound
disp('hello');
opt = optimoptions('quadprog', 'Algorithm', 'interior-point-convex');
%alpha = quadprog(G, f', [], [], Aeq, beq', LB', UP', [], opt);
disp('hello2');
alpha = gsmo(G, f', Aeq', beq, LB', UP');
disp('bye');

margin = 2 / sqrt(sum(sum((alpha * alpha') .* G)));
idx = find(alpha > 1e-8);
length(label(idx)) * sum(label(idx) .* (-alpha(idx)' * G(idx, idx)) + label(idx));
b = 1 / length(label(idx)) * sum(label(idx) .* ...
                                 (-alpha(idx)' * G(idx, idx)) + label(idx));
% Remultiply by label to cancel redundant yi in G
res = label .* (label .* (alpha' * G) + b);
Err = 100 * sum(res < 0) / length(label);

end

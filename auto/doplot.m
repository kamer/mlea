figure;
plot(t(1:length(x_desi)), x_desi, 'b');
hold on;
plot(t(1:length(x_desi)), x_curr, 'g');

figure;
plot(t(1:length(tmp1)), tmp1, 'b');
hold on;
plot(t(1:length(tmp2)), tmp2, 'g');
legend('vitesse', 'omega');

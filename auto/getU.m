function [u1, u2] = getU(t, x_curr, y_curr)
[x, y, dx, dy] = getX(t);
lambda1 = 1;
lambda2 = 1;
u1 = dx + lambda1 * (x - x_curr);
u2 = dy + lambda2 * (y - y_curr);
end

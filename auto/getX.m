function [x, y, dx, dy] = getX(t)
% Centre en metre
c = [20, 20];
% Rayon en metre
R = 5;
% Frequence en hertz
freq = 0.01;
% Pulsation en rad.s^-1
w = 2 * pi * freq;

x = c(1) + R * cos(w*t);
y = c(2) + R * sin(w*t);
dx = -R * w * sin(w*t);
dy = R * w * cos(w*t);
end

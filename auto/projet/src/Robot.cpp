/*
MobileRobots Advanced Robotics Interface for Applications (ARIA)
Copyright (C) 2004, 2005 ActivMedia Robotics LLC
Copyright (C) 2006, 2007, 2008, 2009 MobileRobots Inc.
Copyright (C) 2010, 2011 Adept Technology, Inc.

     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

If you wish to redistribute ARIA under different terms, contact 
Adept MobileRobots for information about a commercial version of ARIA at 
robots@mobilerobots.com or 
Adept MobileRobots, 10 Columbia Drive, Amherst, NH 03031; 800-639-9481
*/
#include "Aria.h"

#define pi 3.141592654
#define kx 5
#define ky 500
#define kth 100
#define Cycle 20
#define Te Cycle*1e-3
#define a 0.1                      //vitesse de trj d�sir�e

/** @example robotSyncTaskExample.cpp  Shows how to add a task callback to ArRobot's synchronization/processing cycle

  This program will just have the robot wander around, it uses some avoidance 
  routines, then just has a constant velocity.  A sensor interpretation task callback is invoked
  by the ArRobot object every cycle as it runs, which records the robot's current 
  pose and velocity.

  Note that tasks must take a small amount of time to execute, to avoid delaying the
  robot cycle.
  Travail refait le 17 Aout 2012
  trajectoire 1 = droite
*/

class PrintingTask
{
public:
  // Constructor. Adds our 'user task' to the given robot object.
  PrintingTask(ArRobot *robot);

  // Destructor. Does nothing.
  ~PrintingTask(void) {}
  
  // This method will be called by the callback functor
  void doTask(void);
protected:
  ArRobot *myRobot;
  FILE *file;
  int i,j;         // indices utiles

  // The functor to add to the robot for our 'user task'.
  ArFunctorC<PrintingTask> myTaskCB;
};


// the constructor (note how it uses chaining to initialize myTaskCB)
PrintingTask::PrintingTask(ArRobot *robot) :
  myTaskCB(this, &PrintingTask::doTask)
{
  myRobot = robot;
  // just add it to the robot
  myRobot->addSensorInterpTask("PrintingTask", 50, &myTaskCB);
  file = ArUtil::fopen("data.dat", "w+");
  i=0;
  j=0;
}

#define LEFT 2
#define LMID 3
#define RMID 4
#define RIGHT 5
//#define MIN 100
//#define MAX 5000
void PrintingTask::doTask(void)
{
double x,y,th,t;                      //variables de sortie
double ex,ey,eth;                   //les erreurs
double v,w;                         //variables de commande
double vr,wr;                       //consignes de vitesse
double l,xt,yt,tht;                 //variables dues aux consignes

   wr=0;
   vr=0.05;
   //calcul des trajectoires d�sir�es
   tht=pi/6;
   t=i*Te;
   l=a*t;
   xt=l*cos(tht)+0.2;
   yt=l*sin(tht)+0.1;
   // MYCODE
   float oldth = myRobot->getTh()*(pi/180);
   float oldx=myRobot->getX()*1e-3;
   float oldy=myRobot->getY()*1e-3; 
   double mya = 0.1;
   double myb = 0.2;
   xt=sin(t*mya);
   yt=sin(t*myb);
   double xx = mya * cos(t*mya);
   double yy = myb * cos(t*myb);
   tht = atan2(yy, xx);
   for (int j = LEFT; j <  RIGHT+1 /*myRobot->getNumSonar()*/; j++)
     {
       //printf(" %4d", myRobot->getSonarRange(j));
     }
   static int count = 0;
   int max = 800;
   // Guessed theta for rays
   int thetas[8] = { -pi/2, -pi/4, -pi/8, -pi/16, pi/16, pi/8, pi/4, pi/2 };
   int index[8] = {  0, 1, 2, 3, 4, 5, 6, 7 };
   float xo[8] = { FLT_MAX, FLT_MAX, FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX };
   float yo[8] = { FLT_MAX, FLT_MAX, FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX };
   // left pi/3, midleft pi/12
   {
     for (int j = 0; j < 8; ++j)
       {
         // 5000 is max vaule meaning no obsticle
         if (myRobot->getSonarRange(j) == 5000)
           continue;
         float theta = thetas[j] + oldth;
         float val = myRobot->getSonarRange(j) * 1e-3;
         xo[j] = oldx + val * cos(theta);
         yo[j] = oldy + val * sin(theta);
       }
   }
   printf("(%f, %f), (%f, %f)\n", xo[4], yo[4], oldx, oldy);
   // ENDMYCODE
   i++;
  // r�cup�rer some info about the robot
   x=myRobot->getX()*1e-3;
   y=myRobot->getY()*1e-3; 
   th=myRobot->getTh()*(pi/180);

   // calcul des erreurs
   ex= (xt-x)*cos(th)+(yt-y)*sin(th) ;
   ey= -(xt-x)*sin(th)+(yt-y)*cos(th);
   eth=tht-th;

   //calcul des consignes
   v=vr*cos(eth)+kx*ex;
   w=wr+vr*(ky*ey+kth*sin(eth));

   // envoi des consignes
  myRobot->lock();
  myRobot->setRotVel(w*(180/pi));
  myRobot->setVel(v*1000);
  myRobot->unlock();
   
  fprintf(file, "%f, %f, %f, %f, %f, %f, %f\n", x,y,th,xt,yt,tht,t);
  // Need sensor readings? Try myRobot->getRangeDevices() to get all 
  // range devices, then for each device in the list, call lockDevice(), 
  // getCurrentBuffer() to get a list of recent sensor reading positions, then
  // unlockDevice().
}

int main(int argc, char** argv)
{
  // the connection
  ArSimpleConnector con(&argc, argv);
  if(!con.parseArgs())
  {
    con.logOptions();
    return 1;
  }

  // robot
  ArRobot robot;

  // sonar array range device
  ArSonarDevice sonar;

  // This object encapsulates the task we want to do every cycle. 
  // Upon creation, it puts a callback functor in the ArRobot object
  // as a 'user task'.
  PrintingTask pt(&robot);

  // the actions we will use to wander
  ArActionStallRecover recover;
  ArActionAvoidFront avoidFront;
  ArActionConstantVelocity constantVelocity("Constant Velocity", 400);

  // initialize aria
  Aria::init();

  // add the sonar object to the robot
  robot.addRangeDevice(&sonar);

  // open the connection to the robot; if this fails exit
  if(!con.connectRobot(&robot))
  {
    printf("Could not connect to the robot.\n");
    return 2;
  }
  printf("Connected to the robot. (Press Ctrl-C to exit)\n");
  
  
  // turn on the motors, turn off amigobot sounds
  robot.comInt(ArCommands::ENABLE, 1);
  robot.comInt(ArCommands::SOUNDTOG, 0);

  // add the wander actions
  /*robot.addAction(&recover, 100);
  robot.addAction(&avoidFront, 50);
  robot.addAction(&constantVelocity, 25);
  */
  robot.setCycleTime(Cycle);
  // Start the robot process cycle running. Each cycle, it calls the robot's
  // tasks. When the PrintingTask was created above, it added a new
  // task to the robot. 'true' means that if the robot connection
  // is lost, then ArRobot's processing cycle ends and this call returns.
  robot.run(true);

  printf("Disconnected. Goodbye.\n");
  
  return 0;
}

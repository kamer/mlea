function [v, w] = getVW(t, x_curr, y_curr, theta)
% Offset de 0.1 metre
[u1, u2] = getU(t, x_curr, y_curr);
l = 1.5;
J = [cos(theta), -l * sin(theta);
     sin(theta),  l * cos(theta)];

res = inv(J) * [u1; u2];
v = res(1);
%v = min(v, 0.5);
w = res(2);
%w = min(w, 0.1);
end
